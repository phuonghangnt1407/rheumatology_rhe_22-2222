%RSA_OBSERVER_STATS compute sensitivity and specificity for
%the grades assigned to nailfold mosaics by observers in the RSA study
%
%   [results, image_grades_by_marker_visit] = ...
%       compute_RSA_observer_stats(visit, tie_pos, do_plot)
%
% Inputs:
%      visit - visit from 1 - 3 from which to compute results. For the paper
%      we are interested in the baseline visit 1
%
%      tie_pos - flag for how to treat ties when consensus grading an
%      image/subject. If true, ties are treated as positive (abnormal),
%      otherwise ties are treated as false.
%
%      do_plot - flag to generate plots. If 1, will generate plot for all
%      marker consensus only. If > 1, will also plot each individual
%      performance
%
%
% Outputs:
%      results - [1×1 struct] with fields:
%           - observer01-10: statistics for each individual observer
%                n_pos: number of positive subjects graded
%                n_neg: number of negative subjects graded
%                grade: [1×1 struct]
%                   ns_neg: [1×1 struct] using non-specific as positive
%                   label
%                   ns_pos: [1×1 struct] using non-specific as negative
%                   label
%                       subject_mean: [1×1 struct] taking the majority over
%                       image grades to grade a subject
%                       subject_max: [1×1 struct] taking the maximum over
%                       image grades to grade a subject
%
%                           sensitivity: true positive rate
%                           specificity: 1 - false positive rate
%
%           - all: statistics for the consensus over all observers
%                n_pos: number of positive subjects graded
%                n_neg: number of negative subjects graded
%                grade: [1×1 struct]
%                   ns_neg: [1×1 struct] using non-specific as positive
%                   label
%                   ns_pos: [1×1 struct] using non-specific as negative
%                   label
%                       subject_mean: [1×1 struct] taking the majority over
%                       image grades to grade a subject
%                       subject_max: [1×1 struct] taking the maximum over
%                       image grades to grade a subject
%                           image_mean: [1×1 struct] taking the majority over
%                           marker grades to grade an image
%                           image_max: [1×1 struct] taking the max over
%                           marker grades to grade an image
%                               sensitivity: true positive rate
%                               specificity: 1 - false positive rate
%              
%
% Example: [results, image_grades_by_marker_visit] = ...
%               compute_RSA_observer_stats(1, 0, 1)
%
% Notes: There are 4 key decisions to make in obtaining sensitivity and
% specificity score for the RSA observer grades:
%
%   1. How to take consensus over markers to grade each image. We use two
%   options: (a) taking the mean grade, equivalent to saying a mosaic is
%   abnormal if a majority of markers grade it abnormal; (b) taking the max
%   grade, equivalent to saying an image is abnormal if any one marker
%   grades it as abnormal. This only applies to computing statistics over
%   all observers, and is not needed for computing statistics for
%   individual observers. The output results contains results for both
%   options.
%
%   2. How to take consensus over images to grade each subject. We use two
%   options: (a) taking the mean grade, equivalent to saying a subject is
%   abnormal if a majority of its images are graded abnormal; (b) taking the max
%   grade, equivalent to saying a subject is abnormal if any one its images
%   are graded abnormal. This option applies both to the consensus over all
%   markers (in which case each image grade is a consensus grade using
%   either of the options in step (1) above.
%   The output results contains results for both options.
%
%   3. How to to deal with ties. For either of the image or subject mean
%   (ie majority grade) approaches we need to choose how to deal with ties
%   when there are an even number of grades and an equal split between
%   positive negative. This is dealt with at the top level of the function
%   by specifying the tie_pos input argument.
%
%   4. How to deal with non-specific grades. These can either be assigned
%   as positive or negative. The output results contains results for both
%   options. Together with the choice of using mean/max for image consensus
%   and mean/max for subject consensus, this generates 4 possible results
%   for each individual performance and 8 possible results for the
%   performance of all obervers. Each of those can themselves be computed
%   using true/false for the tie_pos argument (see (3)) when calling the
%   function.
%
%   For the paper, we show the result for the consensus of all observers,
%   by taking the majority grade over images and then the maximum grade of
%   the image consensus grade over subjects. We propose this is closest to
%   a natural clinical decision making process in accordance with how
%   nailfold assessments are made for diagnosing SSc, however this code
%   allows the user to explore all other options. Plotting the output shows
%   the results form an ROC-curve like shape, and thus suggest the various
%   options can be conceived as shifting performance somewhere along the
%   compromise between sensitivity/specificity. We see that the option we
%   choose (image mean/subject max) lies closest to the equal-error rate,
%   which provides another reason for choosing this option in the paper.
%
% See also:
%
% Created: Oct-2022
% Author: Michael Berks 
% Email : michael.berks@manchester.ac.uk 
% Phone : +44 (0)161 275 5153 
% Copyright: (C) University of Manchester

%%
%Call main function to generate observer statistics
[results, image_grades_by_marker_visit] = ...
    compute_RSA_observer_stats(1, 0, 1);
%%
%Print numeric results quoted in the paper
print_assessment_counts(image_grades_by_marker_visit);
print_expert_performance(results)

%% -----------------------------------------------------------------------
function [results, image_grades_by_marker_visit] = ...
    compute_RSA_observer_stats(visit, tie_pos, do_plot)
%See description in main script comments header above

results = struct();
grades_xlsx = 'RSA_observer_grades_anonymised.xlsx';
markers = {...
    'observer01'
    'observer02'
    'observer03'
    'observer04'
    'observer05'
    'observer06'
    'observer07'
    'observer08'
    'observer09'
    'observer10'};

n_markers = length(markers);

%Get list of subject class and image grades and vessel density for each
%marker
[subject_class, image_grades_by_marker_visit] = ...
    get_all_image_grades(grades_xlsx, markers);
n_subjects = size(image_grades_by_marker_visit, 1);
pos_subjects = subject_class == 2;

%Do the rest of the analysis on the selected visit
image_grades_by_marker = image_grades_by_marker_visit(:,:,:,visit);

%Loop over all 10 subjects computing statistics for each indiviudally,
%on the 11th iteration compute statistics for the group consensus
for i_ma = 1:n_markers+1
    
    %Compute positive/negative subjects for this marker/group
    if i_ma > n_markers
        %Convert groups to positive/negative labels
        results.all.n_pos = sum(pos_subjects);
        results.all.n_neg = n_subjects - results.all.n_pos;
    else
        marker = markers{i_ma}; 
        valid_subjects = any(~isnan(image_grades_by_marker(:,:,i_ma)),2);
            
        pos_subjects_marker = pos_subjects(valid_subjects);     
        results.(marker).n_pos = sum(pos_subjects_marker);
        results.(marker).n_neg = sum(~pos_subjects_marker);
    end
    
    %Get consensus grade for each image by mean or max and by non-specific as
    %positive or negative
    for ns_pos = [0, 1]
        if ns_pos
            ns_pos_field = 'ns_pos';
        else
            ns_pos_field = 'ns_neg';
        end

        %Compute results for taking the mean or max over images for each
        %subject (see note (2) above)
        for subject_consensus_method = {'subject_mean', 'subject_max'}
            subject_method = subject_consensus_method{1};

            if i_ma > n_markers
                %This is for all observers, so need to take consensus over
                %each observer, using either mean or max (see notes (1)
                %above)
                for image_consensus_method = {'image_mean', 'image_max'}
                    image_method = image_consensus_method{1};

                    %First get consensus over images
                    if strcmpi(image_method, 'image_mean')
                        image_consensus = get_image_consensus_mean(...
                            image_grades_by_marker, ns_pos, tie_pos);
                    else
                        image_consensus = get_image_consensus_max(...
                            image_grades_by_marker, ns_pos);
                    end

                    %Then get consensus of subject
                    if strcmpi(subject_method, 'subject_mean')

                        subject_consensus = ...
                            get_subject_consensus_mean(image_consensus, tie_pos);
                    else
                        subject_consensus = ...
                            get_subject_consensus_max(image_consensus);
                    end

                    %Compute sensitivity and specificity
                    results.all.grade.(ns_pos_field).(subject_method).(image_method) = ...
                        compute_sens_spec(subject_consensus, pos_subjects, ...
                        results.all.n_pos, results.all.n_neg);

                end
            else
                %This is for an individual marker - so just need their
                %grades for each image, and apply the relevant subject
                %consensus method
                [marker_grades] = get_marker_grades(...
                    image_grades_by_marker(valid_subjects,:,i_ma), ns_pos);
            
                if strcmpi(subject_method, 'subject_mean')
                    subject_consensus = ...
                        get_subject_consensus_mean(marker_grades, tie_pos);
                else
                    subject_consensus = ...
                        get_subject_consensus_max(marker_grades);
                end
                results.(marker).grade.(ns_pos_field).(subject_method) = ...
                    compute_sens_spec(subject_consensus, pos_subjects_marker, ...
                    results.(marker).n_pos, results.(marker).n_neg);
            end
        end
    end
    
    %Plot the output if flagged
    if do_plot
        if i_ma > n_markers
            plot_results_all(results.all);
        elseif do_plot > 1
            plot_results_marker(results.(marker), marker);
        end
    end
end
    

end

%% ----------------------------------------------------------------------
function [r] = compute_sens_spec(...
    subject_consensus, pos_subjects, n_pos, n_neg)
%Given the consensus grade for each subject, compute the overall sensitivity 
%and specificity of the grades vs the ground truth subject group label

%Inputs:
%   subject_consensus - (n_subjects x 1) array containing consensus grade
%   for each subject
%
%   pos_subjects - (n_subjects x 1) array containing ground truth label for
%   each subject
%
%   n_pos - number of positive labels
%
%   n_neg - number of negative labels
%
%Outputs:
%   r - structure containing fields
%       - sensitivity
%       - specificity
    r = struct();
    r.sensitivity = sum(subject_consensus & pos_subjects) / n_pos; 
    r.specificity = sum(~subject_consensus & ~pos_subjects) / n_neg;
end


%% ----------------------------------------------------------------------
function plot_results_all(results)
%Plot sensitivity vs (1 - specificity) for each result type for a 
%the consensus over all markers 

    figure; 
    axis equal; axis([0 1 0 1]);
    title(['Mean of all markers (N_{SSc} = ' num2str(results.n_pos)...
        ', N_{HC+PR} = ' num2str(results.n_neg) ')']);
    xlabel('1 - specificity')
    ylabel('Sensitivity');
    hold all;
    
    legend_labels = cell(1,0);  
    for ns_pos_method = {'ns_pos', 'ns_neg'}
        ns_method = ns_pos_method{1};
        if strcmpi(ns_method, 'ns_pos')
            ns_str = 'abnormal';
        else
            ns_str = 'normal';
        end
        
        for subject_consensus_method = {'subject_mean', 'subject_max'}
            subject_method = subject_consensus_method{1};
            
            subject_str = subject_method;
            subject_str(subject_str == '_') = ' ';

            for image_consensus_method = {'image_mean', 'image_max'}
                image_method = image_consensus_method{1};
                
                image_str = image_method;
                image_str(image_str == '_') = ' ';
                
                r = results.grade.(ns_method).(subject_method).(image_method);
                plot(1 - r.specificity, r.sensitivity(1), ...
                    '*', 'markersize', 8);
                
                legend_str = sprintf(...
                    'Image grade (NS = %s, %s, %s)',...
                    ns_str, subject_str, image_str);
                
                legend_labels{end+1} = legend_str; %#ok
            end
        end
    end
    
    legend(legend_labels, 'location', 'southeast');
end

%% ----------------------------------------------------------------------
function plot_results_marker(results, marker)
%Plot sensitivity vs (1 - specificity) for each result type for a 
%given marker 
    figure;  
    axis equal; axis([0 1 0 1]);
    title([marker ': (N_{SSc} = ' num2str(results.n_pos)...
        ', N_{HC+PR} = ' num2str(results.n_neg) ')']);
    xlabel('1 - specificity')
    ylabel('Sensitivity');
    hold all;
    
    legend_labels = cell(1,0);
    for ns_pos_method = {'ns_pos', 'ns_neg'}
        ns_method = ns_pos_method{1};
        if strcmpi(ns_method, 'ns_pos')
            ns_str = 'abnormal';
        else
            ns_str = 'normal';
        end
        
        for subject_consensus_method = {'subject_mean', 'subject_max'}
            subject_method = subject_consensus_method{1};
            
            subject_str = subject_method;
            subject_str(subject_str == '_') = ' ';
                
            r = results.grade.(ns_method).(subject_method);
            plot(1 - r.specificity, r.sensitivity(1), ...
                '*', 'markersize', 8);
                
            legend_str = sprintf(...
                'Image grade (NS = %s, %s)',...
                ns_str, subject_str);

            legend_labels{end+1} = legend_str; %#ok
     
        end
    end
    
    legend(legend_labels, 'location', 'southeast');
end

%% ----------------------------------------------------------------------
function [subject_class, image_grades_by_marker_visit] = ...
    get_all_image_grades(grades_xlsx, markers)
%Create an array of marker grades from the input Excel spreadsheet. The
%spreadsheet is assumed to have separate sheets for each observer

    %Pre-allocate grades array. Dimensions hard-coded to the is the numbers we expect, 
    %but the code allows this to dynamically grow/shrink as necessary
    num_markers = length(markers);
    n_subjects = 173; 
    n_digits = 10;
    n_visits = 3;
    
    image_grades_by_marker_visit = nan(...
        n_subjects, n_digits, num_markers, n_visits);
    
    %Pre-allocate subject class array
    subject_class = zeros(n_subjects, 1);

    for i_ma = 1:num_markers

        %Load grading data for this marker from the matching marker sheet
        %in the XLSX file of grades
        marker = markers{i_ma};
        [~,~, all_data] = xlsread(grades_xlsx, marker);
        
        %First row of column headers can be discarded
        all_data(1,:) = [];
        
        %Get subject_ids, visits, group_labels
        subject_ids = all_data(:,4);
        subject_groups = all_data(:, 5);
        
        %Find set of rows that have valid IDs, group label 'HC', 'P' or 'S'
        %and match this visit
        [subject_ids, subject_groups, selected_rows] = ...
            select_rows(subject_ids, subject_groups);
            
        %Visits are 6th column
        subject_visits = all_data(selected_rows,6);
        
        %Hands are in 7th column
        hands = all_data(selected_rows,7);
        
        %Digit (1 - 5) are in 8th column
        digits = all_data(selected_rows, 8);
        
        %Image grades  are in the 9th column
        grades = all_data(selected_rows, 9);
        n_images = length(subject_ids);

        %Loop over all selected images
        for i_im = 1:n_images

            %Get subject ID
            subject_id = subject_ids(i_im);

            %Get subject group
            subject_class(subject_id,1) = ...
                get_image_group(subject_groups{i_im});

            %Get image digit as 1 - 10
            h_digit = get_digit(hands{i_im}, digits{i_im});
            visit = subject_visits{i_im};

            %Get image-marker grade
            image_grades_by_marker_visit(subject_id, h_digit, i_ma, visit) = ...
                get_image_grade(grades{i_im});

        end
    end
    
    %Discard subjects with no gradeable images
    valid_subjects = any(any(any(image_grades_by_marker_visit >= 0, 4), 3),2);

    subject_class(~valid_subjects) = [];
    image_grades_by_marker_visit(~valid_subjects,:,:,:) = [];
end

%% ----------------------------------------------------------------------
function [subject_ids, subject_groups, selected_rows] = ...
    select_rows(subject_ids, subject_groups)

    valid_id = cellfun('isclass',subject_ids,'double');
    subject_groups_valid = subject_groups(valid_id);
    
    
    valid_group = strcmpi(subject_groups_valid, 'HC') | ...
            strcmpi(subject_groups_valid, 'S') | ...
            strcmpi(subject_groups_valid, 'P');
    
    selected_rows = valid_id;
    selected_rows(valid_id) = valid_group;
    
    subject_ids = cell2mat(subject_ids(selected_rows));
    subject_groups = subject_groups(selected_rows);
    
end

%% ----------------------------------------------------------------------
function [grade] = get_image_grade(grade_str)
%Match the text form grade in grade_str to a numerical grade we use in
%further analysis
    switch grade_str
        case 'Normal'
            grade = 0;
        case 'Early'
            grade = 2;
        case 'Active'
            grade = 3;
        case 'Late'
            grade = 4;
        case {'Non-specific'}
            grade = 1;
        case 'Ungradeable_Condition'
            grade = 5;
        case 'Ungradeable_Quality'
            grade = -1;
        case {'Undefined', 'U'}
            grade = -2;
        otherwise
            warning(['No grade matched to ' grade_str]);
            grade = -3;
    end
end

%% ----------------------------------------------------------------------
function group = get_image_group(group_str)
%Match text form group in group_str to a numerical group code we use in
%further analysis
    switch group_str
        case 'HC'
            group = 0;
        case 'P'
            group = 1;
        case 'S'
            group = 2;
        otherwise
            warning(['No group matched to ' group_str]);
            group = NaN;
    end
end

%% ----------------------------------------------------------------------
function h_digit = get_digit(hand, digit)
%Convert letter coded hand and digit from 1 - 5 into a hand_digit from 1 -
%10, where L1-5 -> 1-5 and R1-5 -> 6-10
    if strcmpi(hand, 'L')
        hand = 0;
    else
        hand = 1;
    end
    h_digit = 5*hand + digit;
end

%% ----------------------------------------------------------------------
function image_consensus = ...
    get_image_consensus_mean(image_grades_by_marker, ns_pos, tie_pos)
%Get a consensus score for an image by taking the mean over the marker
%grades. This equivalent to using the majority grade

%Inputs:
%   image_grades_by_marker - 3D array (n_subjects x n_digits x n_markers)
%   containing marker grades for each mosaic
%
%   ns_pos - flag for how to treat non-specific (NS) grade. If true, NS are
%   treated as positive (abnormal). If false, treated as negative.
%
%   tie_pos - lag for how to treat ties between markers (ie when an equal
%   number of markers assign positive and negative to an image). If true,
%   ties are graded positive (abnormal). If false, ties are negative
%
%Outputs:
%   image_consensus - consensus grade for each image (n_subjects x
%   n_digits)

    %Count how many gradeable images there were - note missing have NaN
    %grade, so will return negative and thus won't be counted
    gradeable = image_grades_by_marker >= 0;
    n_gradeable_markers = sum(gradeable, 3);
    
    %Count how many markers gave a positive grade. If ns_pos == 1, then a
    %positive grade is any > 0. If ns_pos == 0, a positive grade must be >
    %1. As above missing grades are NaN and won't be counted
    image_pos_by_marker = image_grades_by_marker > (1 - ns_pos);
    n_pos_markers = sum(image_pos_by_marker, 3);
    
    %The consensus is computed by taking the mean of the gradeable labels.
    %Ties at 0.5 can either be graded negative or positive
    image_consensus_mean = n_pos_markers ./ n_gradeable_markers;
    if tie_pos
        image_consensus = image_consensus_mean >= 0.5;
    else
        image_consensus = image_consensus_mean > 0.5;
    end
    
    %Images with no gradeable markers should be set to consensus score NaN
    %so they are not included in subject classification
    image_consensus = double(image_consensus);
    image_consensus(~n_gradeable_markers) = NaN;
    
end

%% ----------------------------------------------------------------------
function image_consensus = ...
    get_image_consensus_max(image_grades_by_marker, ns_pos)
%Get a consensus score for an image by taking the maximum over the marker
%grades. This equivalent to saying an image is abnormal if any marker
%grades it as abnormal.

%Inputs:
%   image_grades_by_marker - 3D array (n_subjects x n_digits x n_markers)
%   containing marker grades for each mosaic
%
%   ns_pos - flag for how to treat non-specific (NS) grade. If true, NS are
%   treated as positive (abnormal). If false, treated as negative.
%
%
%Outputs:
%   image_consensus - consensus grade for each image (n_subjects x
%   n_digits)

    %Check for any positive grade. If ns_pos == 1, then a
    %positive grade is any > 0. If ns_pos == 0, a positive grade must be >
    %1. As above missing grades are NaN and won't be counted
    image_pos_by_marker = image_grades_by_marker > (1 - ns_pos);
    image_consensus = any(image_pos_by_marker, 3);
    
    %Images with no gradeable markers should be set to consensus score NaN
    %so they are not included in subject classification
    image_consensus = double(image_consensus);
    image_consensus(~any(image_grades_by_marker >= 0,3)) = NaN;
end

%% ----------------------------------------------------------------------
function subject_consensus = ...
    get_subject_consensus_mean(image_grades, tie_pos)
%Get a consensus score for an subject by taking the mean over the image
%grades for each mosaic belonging to the subject. This equivalent to using 
%the majority grade for the subject

%Inputs:
%   image_grades - 3D array (n_subjects x n_digits)
%   containing grades for each mosaic
%
%
%   tie_pos - lag for how to treat ties (ie when an equal
%   number of images are positive and negative for a subject). If true,
%   ties are graded positive (abnormal). If false, ties are negative
%
%Outputs:
%   subject_consensus - consensus grade for each image (n_subjects x 1)

    %Subject-visit classified as positive if the mean label of it's graded images
    % are >(=) 0.5. Ties at 0.5 can split positive or negative
    n_graded = sum(~isnan(image_grades), 2);
    n_pos = nansum(image_grades, 2);
    mean_grade = n_pos ./ n_graded;
    if tie_pos
        subject_consensus = mean_grade >= 0.5;
    else
        subject_consensus = mean_grade > 0.5;
    end
end

%% ----------------------------------------------------------------------
function subject_consensus = ...
    get_subject_consensus_max(image_grades)
%Get a consensus score for an subject by taking the max over the image
%grades for each mosaic belonging to the subject. This equivalent to saying
%a subject is abnormal if any one of their images is abnormal and is
%closest to how clinical decisions are made.

%Inputs:
%   image_grades - 3D array (n_subjects x n_digits) containing grades for 
%   each mosaic
%
%Outputs:
%   subject_consensus - consensus grade for each image (n_subjects x 1)

    %Subject-visit classified as positive if any image has positive label
    %NaNs can safely be ignored
    subject_consensus = any(image_grades, 2);
end

%% ----------------------------------------------------------------------
function [image_grades] = ...
    get_marker_grades(marker_grades, ns_pos)
    %Check for any positive grade. If ns_pos == 1, then a
    %positive grade is any > 0. If ns_pos == 0, a positive grade must be >
    %1. As above missing grades are NaN and won't be counted
    graded = ~isnan(marker_grades);
    
    image_grades = double(marker_grades > (1 - ns_pos));
    image_grades(~graded) = NaN;
end

%% ----------------------------------------------------------------------
function print_assessment_counts(image_grades_by_marker_visit)
    total_assessments = nnz(image_grades_by_marker_visit(:,:,:,1) >= -1);
    fprintf('\n----------------------------------------------------\n');
    fprintf('Total assessments = %d\n', total_assessments);
    fprintf('Numbers in each label category:\n');
    %Labels in the order we present them in the paper
    label_names = {
        'Normal'
        'Early'
        'Active' 
        'Late'
        'Ungradeable condition'
        'Non-specific'
        'Ungradeable quality'};

    label_ids = [0 2:5 1 -1];
    for i_g = 1:length(label_names)
       label_count = ...
           nnz(image_grades_by_marker_visit(:,:,:,1) == label_ids(i_g)); 
       fprintf('    %s: %d\n', label_names{i_g}, label_count);
    end

    image_grade_counts = sum(image_grades_by_marker_visit(:,:,:,1) >= 0,3);
    fprintf('\nImages with at least one marker grade = %d\n', ...
        nnz(image_grade_counts));
    fprintf('Images with only one marker grade = %d\n', ...
        nnz(image_grade_counts == 1));
    fprintf('Images with two or more marker grades = %d\n', ...
        nnz(image_grade_counts >= 2));
end
%% ----------------------------------------------------------------------
function print_expert_performance(results)
    fprintf('\n----------------------------------------------------\n');
    fprintf('Expert performance\n');
    r = results.all.grade.ns_neg.subject_max.image_mean;
    fprintf('   Sensitivity = %2.0f%%\n', 100*r.sensitivity);
    fprintf('   Specificity = %2.0f%%\n', 100*r.specificity);
    
    res_file = fullfile(...
        fileparts(matlab.desktop.editor.getActiveFilename),...
        '..', 'deep_learning_predictions', 'expert_performance.txt');

    fid = fopen(res_file, 'wt');
    fprintf(fid, '%5.4f %5.4f', r.sensitivity, r.specificity);
    fclose(fid);
end


