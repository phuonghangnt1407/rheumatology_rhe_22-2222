'''
This script makes the ROC curves plot shown in Figure 1(d) of 
our paper Rheumatology RHE 22-2222.

It should be called in an environment with numpy and matplotlib installed
but otherwise requires no special libraries.
'''
#%%
import matplotlib.pyplot as plt
import numpy as np

#Load in results computed from the Matlab analysis code, see
#   log_reg_analysis.m and
#   ../expert_performance/RSA_observer_stats.m
#for details of how these are computed
groupB_roc = 100*np.loadtxt('groupB_rocs.txt')
groupC_roc = 100*np.loadtxt('groupC_rocs.txt')

expert_performance = 100*np.loadtxt('expert_performance.txt')
expert_sens = expert_performance[0]
expert_spec = expert_performance[1]

#%%
#Create the ROC plots
plt.figure(figsize = (12,12))
h_B, = plt.plot(groupB_roc[:,0], groupB_roc[:,1], 'b', linewidth = 4)
h_BCI = plt.fill_between(groupB_roc[:,0], groupB_roc[:,2], groupB_roc[:,3],
    color = 'b', alpha = 0.2)
h_C, = plt.plot(groupC_roc[:,0], groupC_roc[:,1], 'r', linewidth = 4)
h_CCI = plt.fill_between(groupC_roc[:,0], groupC_roc[:,2], groupC_roc[:,3],
    color = 'r', alpha = 0.2)

h_ex, = plt.plot(100 - expert_spec, expert_sens, 'ko', 
    markersize = 20, markerfacecolor = 'k')
h_eer, = plt.plot([0,100],[100,0], 'k--')
plt.xlabel('100 - specificity (%)', fontsize = 24)
plt.ylabel('Sensitivity (%)', fontsize = 24)
plt.axis((0,100,0,100))

plt.xticks(fontsize=16)
plt.yticks(fontsize=16)

expert_label = 'Expert consensus'
plt.legend(
    (h_B, h_BCI, h_C, h_CCI, h_ex, h_eer),
    ('Group B median',
    'Group B 90% CI',
    'Group C median',
    'Group C 90% CI',
    expert_label,
    'EER line'), 
    fontsize = 20
)
plt.savefig('groupBC_roc_curves.png', bbox_inches='tight', facecolor='w')
